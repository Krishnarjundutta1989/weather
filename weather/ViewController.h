//
//  ViewController.h
//  weather
//
//  Created by click labs 115 on 10/15/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UISearchBarDelegate>
@property (nonatomic,strong) NSString *city;
@property (strong, nonatomic) IBOutlet UISearchBar *searchCity;



@end

