//
//  ShowInDetailsViewController.h
//  weather
//
//  Created by click labs 115 on 10/20/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowInDetailsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblResult;
@property (strong,nonatomic) NSMutableDictionary *strShowDataInDestination;
@property (strong,nonatomic) NSMutableArray *data;


@end
