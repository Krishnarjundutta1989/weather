//
//  DetailedWeatherViewController.h
//  weather
//
//  Created by click labs 115 on 10/15/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowInDetailsViewController.h"


@interface DetailedWeatherViewController : UIViewController<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tblhour;

@end
