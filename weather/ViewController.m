//
//  ViewController.m
//  weather
//
//  Created by click labs 115 on 10/15/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSMutableArray *days;
    NSMutableArray *dates;
    NSArray *jsonList2;

}
@property (strong, nonatomic) IBOutlet UITabBarItem *tabWeather;
@property (strong, nonatomic) IBOutlet UIImageView *imgBack;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentTemp;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentSkyStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblSlash;
@property (strong, nonatomic) IBOutlet UILabel *lblTempmin;
@property (strong, nonatomic) IBOutlet UILabel *lblTempMax;
@property (strong, nonatomic) IBOutlet UILabel *lblHumidity;
@property (strong, nonatomic) IBOutlet UILabel *lblwind;
@property (strong, nonatomic) IBOutlet UILabel *lbl1;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBarController.tabBar.barTintColor =  [UIColor colorWithRed:10/255.0 green:10/255.0 blue:10/255.0 alpha:0];
    _imgBack.animationImages = [NSArray arrayWithObjects: [UIImage imageNamed:@"1.jpg"], [UIImage imageNamed:@"2.jpg"], [UIImage imageNamed:@"3.jpg"],[UIImage imageNamed:@"4.jpg"], [UIImage imageNamed:@"5.jpg"], [UIImage imageNamed:@"6.jpg"],[UIImage imageNamed:@"7.jpg"], [UIImage imageNamed:@"8.jpg"], [UIImage imageNamed:@"9.jpg"],[UIImage imageNamed:@"10.jpg"], [UIImage imageNamed:@"011.jpg"], [UIImage imageNamed:@"012.jpg"],[UIImage imageNamed:@"013.jpg"], [UIImage imageNamed:@"014.jpg"], [UIImage imageNamed:@"015.jpg"],[UIImage imageNamed:@"016.jpg"], [UIImage imageNamed:@"017.jpg"], [UIImage imageNamed:@"018.jpg"],[UIImage imageNamed:@"019.jpg"], [UIImage imageNamed:@"020.jpg"],[UIImage imageNamed:@"021.jpg"], [UIImage imageNamed:@"022.jpg"], [UIImage imageNamed:@"023.jpg"],[UIImage imageNamed:@"024.jpg"], [UIImage imageNamed:@"025.jpg"], [UIImage imageNamed:@"026.jpg"],[UIImage imageNamed:@"027.jpg"], [UIImage imageNamed:@"028.jpg"], nil];
    _imgBack.animationDuration = 2.0f;
    [_imgBack startAnimating];
    //double lat = -0.13.doubleValue;
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSString *city = _searchCity.text;
    NSString *URL = [NSString
                     stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?q=%@,india&appid=bd82977b86bf27fb59a04b61b657fb6f",city];
    NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    NSString *URL2 = [NSString
                     stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?q=%@&mode=json&units=metric&cnt=7&appid=bd82977b86bf27fb59a04b61b657fb6f",city];
    NSData* data2 = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL2]];
    
    NSDictionary *json2 = [NSJSONSerialization JSONObjectWithData:data2 options:kNilOptions error:nil];

    
    NSDictionary *jsonMain = json[@"main"];
    //NSLog(@"%@",jsonMain[@"temp_max"]);
    NSString *jsonTemp = jsonMain[@"temp"];
    if (jsonTemp !=0 )
    {
        double celsius = jsonTemp.doubleValue  - 273.15;
        NSString *temp = [NSString stringWithFormat:@"%.0f°C",celsius];
        
        
        _lblCurrentTemp.text = temp;
        
    }

    NSDictionary *jsonCoord = json[@"coord"];
    NSString *jsonLon = jsonCoord[@"lon"];
    NSString *jsonLat = jsonCoord[@"lat"];
    //double lon = jsonLon.doubleValue;
    //double lat = jsonLat.doubleValue;
//    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(double lat, double lon);
//    GMSMarker *marker = [GMSMarker markerWithPosition:position];
//    marker.title = @"Welcome Chandigarh";
//    marker.map = _googleMap;

    NSArray *jsonWeather = json[@"weather"];
      for (int i =0;i<jsonWeather.count;i++) {
        NSDictionary *jsondescription = jsonWeather[i];
        if(jsondescription[@"description"]){
            _lblCurrentSkyStatus.text = [NSString stringWithFormat:@" %@ ", jsondescription[@"description"]];
        }
    }

    NSString *jsonMaxTemp = jsonMain[@"temp_max"];
    if (jsonTemp !=0 )
    {
        double celsius = jsonMaxTemp.doubleValue  - 273.15;
        NSString *jsonMaxTemp = [NSString stringWithFormat:@"%.0f°C/",celsius];
        
        
        _lblTempMax.text = jsonMaxTemp;
        
    }

    NSString *jsonMinTemp = jsonMain[@"temp_min"];
    if (jsonTemp !=0 )
    {
        double celsius = jsonMinTemp.doubleValue  - 273.15;
        NSString *jsonMinTemp = [NSString stringWithFormat:@"%.0f°C",celsius];
        
        
        _lblTempmin.text = jsonMinTemp;
        
    }
    
    NSString *jsonhumidity = jsonMain[@"humidity"];
     _lblHumidity.text = [NSString stringWithFormat:@"%@",jsonhumidity];

    NSDictionary *jsonWind = json[@"wind"];
    NSString *jsonSpeed = jsonWind[@"speed"];
    
    _lblwind.text = [NSString stringWithFormat:@"%@ km/hn",jsonSpeed];
    
    
    
    jsonList2 = json2[@"list"];
    
    days = [NSMutableArray new];
    dates = [NSMutableArray new];
    
    
    for (int i =0;i<jsonList2.count;i++) {
        
        NSDictionary *weather = [jsonList2 objectAtIndex: i];
        NSDictionary *temp = weather[@"temp"];
        NSString *day = temp[@"day"];
        NSString *night = temp[@"night"];
        NSString *eve = temp [@"eve"];
        NSString * morn = temp[@"morn"];
        NSString *dt = weather[@"dt"];
        
        NSString * timeStampString =[NSString stringWithFormat:@"%@",dt];
        NSTimeInterval _interval=[timeStampString doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
        [_formatter setDateFormat:@"dd/MM/yy"];
        [_formatter stringFromDate:date];
        
        [days addObject:[NSString stringWithFormat:@"Morning :         %@ °C",morn]];
        [days addObject:[NSString stringWithFormat:@"Day :                 %@ °C",day]];
        [days addObject:[NSString stringWithFormat:@"Evening :          %@ °C",eve]];
        [days addObject:[NSString stringWithFormat:@"Night :              %@ °C",night]];
        
        if ([days objectAtIndex:0] ) {
            _lbl1.text = [NSString stringWithFormat:@"Morning :         %@ °C",morn];
        }
        
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
