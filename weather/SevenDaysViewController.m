
//  SevenDaysViewController.m
//  weather
//
//  Created by click labs 115 on 10/20/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "SevenDaysViewController.h"

@interface SevenDaysViewController (){
    
    NSMutableArray *days;
    NSMutableArray *dates;
    NSMutableArray *jsonList;
    NSMutableDictionary *strShowDataInDestination;

    
}
@property (strong, nonatomic) IBOutlet UIImageView *imgBack;
@property (strong, nonatomic) IBOutlet UILabel *lblSub;
@property (strong, nonatomic) IBOutlet UISearchBar *searchCity;
@property (nonatomic,strong) NSString *city;
@end

@implementation SevenDaysViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
    [self.navigationController.navigationBar setTranslucent:NO];
    _lblSub.alpha = .06;
    _imgBack.animationImages = [NSArray arrayWithObjects: [UIImage imageNamed:@"1.jpg"], [UIImage imageNamed:@"2.jpg"], [UIImage imageNamed:@"3.jpg"],[UIImage imageNamed:@"4.jpg"], [UIImage imageNamed:@"5.jpg"], [UIImage imageNamed:@"6.jpg"],[UIImage imageNamed:@"7.jpg"], [UIImage imageNamed:@"8.jpg"], [UIImage imageNamed:@"9.jpg"],[UIImage imageNamed:@"10.jpg"], [UIImage imageNamed:@"011.jpg"], [UIImage imageNamed:@"012.jpg"],[UIImage imageNamed:@"013.jpg"], [UIImage imageNamed:@"014.jpg"], [UIImage imageNamed:@"015.jpg"],[UIImage imageNamed:@"016.jpg"], [UIImage imageNamed:@"017.jpg"], [UIImage imageNamed:@"018.jpg"],[UIImage imageNamed:@"019.jpg"], [UIImage imageNamed:@"020.jpg"],[UIImage imageNamed:@"021.jpg"], [UIImage imageNamed:@"022.jpg"], [UIImage imageNamed:@"023.jpg"],[UIImage imageNamed:@"024.jpg"], [UIImage imageNamed:@"025.jpg"], [UIImage imageNamed:@"026.jpg"],[UIImage imageNamed:@"027.jpg"], [UIImage imageNamed:@"028.jpg"], nil];
    _imgBack.animationDuration = 2.0f;
    [_imgBack startAnimating];
    NSString *URL = [NSString
                     stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?q=delhi&mode=json&units=metric&cnt=7&appid=bd82977b86bf27fb59a04b61b657fb6f"];
    NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
    
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    jsonList = json[@"list"];
        days = [NSMutableArray new];
    dates = [NSMutableArray new];
    
    for (int i =0;i<jsonList.count;i++) {
        
        NSDictionary *weather = [jsonList objectAtIndex: i];
        NSDictionary *temp = weather[@"temp"];
        NSString *min = temp[@"min"];
        NSString *max = temp[@"max"];
                NSString *dt = weather[@"dt"];
        
        
             NSString * timeStampString =[NSString stringWithFormat:@"%@ ",dt];
        NSTimeInterval _interval=[timeStampString doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
        [_formatter setDateFormat:@"dd/MM/yy"];
        [_formatter stringFromDate:date];
        
        [days addObject:[NSString stringWithFormat:@"%@ °C/%@ °C",max,min]];
       // [days addObject:[NSString stringWithFormat:@"%@ °C",max]];

        for (int j = 0; j<1; j++) {
            [dates addObject:[NSString stringWithFormat:@"%@", [_formatter stringFromDate:date]]];
        }
        
        
        
        
        NSLog(@"%@",days);
        
        
        
    }
    
    [_tblhour reloadData];
    
      // Do any additional setup after loading the view.
    
}
/*- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
 NSString *city = _searchCity.text;
 NSString *URL = [NSString
 stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast/daily?q=%@&mode=json&units=metric&cnt=7&appid=bd82977b86bf27fb59a04b61b657fb6f",city];
 NSData* data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
 
 NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
 jsonList = json[@"list"];
 //for ( NSDictionary *weather in jsonList )
 //    {
 //        NSDictionary *temp = weather[@"temp"];
 
 //  NSLog(@"Title: %@", temp );
 //        NSLog(@"Speaker: %@", theCourse[@"speaker"] );
 //        NSLog(@"Time: %@", theCourse[@"time"] );
 //        NSLog(@"Room: %@", theCourse[@"room"] );
 //        NSLog(@"Details: %@", theCourse[@"details"] );
 //        NSLog(@"----");
 //}
 
 //NSString *jsonDate = json[@"dt"];
 
 for (int i =0;i<jsonList.count;i++) {
 
 NSDictionary *weather = [jsonList objectAtIndex: i];
 NSDictionary *temp = weather[@"temp"];
 NSString *day = temp[@"day"];
 NSString *night = temp[@"night"];
 NSString *eve = temp [@"eve"];
 NSString * morn = temp[@"morn"];
 
 [days addObject:[NSString stringWithFormat:@"%@",day]];
 [days addObject:[NSString stringWithFormat:@"%@",night]];
 [days addObject:[NSString stringWithFormat:@"%@",eve]];
 [days addObject:[NSString stringWithFormat:@"%@",morn]];
 
 NSLog(@"%@",days);
 
 
 
 }
 }*/

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return days.count;
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse"];
        UILabel *lblDate =(UILabel*) [cell viewWithTag:1];
    lblDate.text =  dates [indexPath.row] ;
    ([cell.contentView addSubview:lblDate]);
    
    
    UILabel *lblTemp =(UILabel*) [cell viewWithTag:2];
    lblTemp.text =  days [indexPath.row] ;
    ([cell.contentView addSubview:lblTemp]);
    
    
    
    
    return cell;
    //    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"goToShowInDetails"] ) {
        
        NSIndexPath *indexPath = [_tblhour indexPathForSelectedRow ];
        ShowInDetailsViewController *destViewController = segue.destinationViewController;
        destViewController.strShowDataInDestination = [jsonList objectAtIndex:indexPath.row];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
