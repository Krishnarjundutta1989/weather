//
//  PrecipitationForecastViewController.m
//  weather
//
//  Created by click labs 115 on 10/15/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "PrecipitationForecastViewController.h"

@interface PrecipitationForecastViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imgBack;

@end

@implementation PrecipitationForecastViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _imgBack.animationImages = [NSArray arrayWithObjects: [UIImage imageNamed:@"1.jpg"], [UIImage imageNamed:@"2.jpg"], [UIImage imageNamed:@"3.jpg"],[UIImage imageNamed:@"4.jpg"], [UIImage imageNamed:@"5.jpg"], [UIImage imageNamed:@"6.jpg"],[UIImage imageNamed:@"7.jpg"], [UIImage imageNamed:@"8.jpg"], [UIImage imageNamed:@"9.jpg"],[UIImage imageNamed:@"10.jpg"], [UIImage imageNamed:@"011.jpg"], [UIImage imageNamed:@"012.jpg"],[UIImage imageNamed:@"013.jpg"], [UIImage imageNamed:@"014.jpg"], [UIImage imageNamed:@"015.jpg"],[UIImage imageNamed:@"016.jpg"], [UIImage imageNamed:@"017.jpg"], [UIImage imageNamed:@"018.jpg"],[UIImage imageNamed:@"019.jpg"], [UIImage imageNamed:@"020.jpg"],[UIImage imageNamed:@"021.jpg"], [UIImage imageNamed:@"022.jpg"], [UIImage imageNamed:@"023.jpg"],[UIImage imageNamed:@"024.jpg"], [UIImage imageNamed:@"025.jpg"], [UIImage imageNamed:@"026.jpg"],[UIImage imageNamed:@"027.jpg"], [UIImage imageNamed:@"028.jpg"], nil];
    _imgBack.animationDuration = 2.0f;
    [_imgBack startAnimating];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
